package ve.net.dcs.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttachment;
import org.compiere.model.MColumn;
import org.compiere.model.MProcess;
import org.compiere.model.MTable;
import org.compiere.model.Query;
import org.compiere.model.X_AD_ReportView_Col;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

public class GenerateFileFromFormatPrinterForm extends SvrProcess {

	public GenerateFileFromFormatPrinterForm() {
		// TODO Auto-generated constructor stub
	}

	/** Record_ID */
	private int p_Record_ID = 0;
	private String p_FormatType = "";
	private String p_FileName = "";
	private int p_AD_Process_ID = 0;
	private String p_TableNameToAttach = "";
	String tableName = "";
	private int m_created = 0;
	private MPrintFormat printFormat = null;
	private String p_Extension = "";

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		p_Record_ID = getRecord_ID();

		ProcessInfoParameter[] para = getParameter();

		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("AD_Process_ID"))
				p_AD_Process_ID = para[i].getParameterAsInt();
			else if (name.equals("TableNameToAttach"))
				p_TableNameToAttach = para[i].getParameterAsString();
			else if (name.equals("FormatType")) {
				if (para[i].getParameterAsString() != null)
					p_FormatType = para[i].getParameterAsString();
			} else if (name.equals("FileName")) {
				if (para[i].getParameterAsString() != null)
					p_FileName = para[i].getParameterAsString();
			} else if (name.equals("Extension"))
				p_Extension = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		File file = null;

		StringBuilder msglog = new StringBuilder("AD_Process_ID=").append(p_AD_Process_ID).append(" Record_ID=")
				.append(p_Record_ID);
		if (log.isLoggable(Level.INFO))
			log.info(msglog.toString());

		if (p_AD_Process_ID <= 0) {
			throw new IllegalArgumentException(Msg.translate(Env.getCtx(), "Process Required!"));
		}

		MProcess formatProcess = MProcess.get(Env.getCtx(), p_AD_Process_ID);

		file = GenerateFile(formatProcess);

		if (toAttach(file)) {
			log.log(Level.SEVERE, "File Name: " + file.getName());
		} else {
			

		}
		StringBuilder msgreturn = new StringBuilder("@Created@ = ").append(m_created);
		return msgreturn.toString();
	}

	private File GenerateFile(MProcess p_formatProcess) {
		File p_file = null;
		StringBuffer sql = new StringBuffer();
		;
		StringBuffer body = new StringBuffer();
		StringBuffer line = new StringBuffer();
		printFormat = (MPrintFormat) p_formatProcess.getAD_PrintFormat();
		String whereClause = "";
		String OrderByClause = "";
		boolean replaceColumn = false;
		Vector<KeyNamePair> vSortNo = new Vector<KeyNamePair>();
		Vector<MPrintFormatItem> vMPrintFormatItem = new Vector<MPrintFormatItem>();
		List<X_AD_ReportView_Col> reportViewCol = null;

		if (p_FileName == null)
			throw new AdempiereException(
					Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "FileName"));
		else if (printFormat == null)
			throw new AdempiereException(
					Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "PrintFormat"));

		if (p_Extension.equals(null))
			throw new AdempiereException(
					Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "p_Extension"));

		p_file = new File(p_FileName.concat(p_Extension));

		if (printFormat.getAD_ReportView().getName() != null) {
			tableName = printFormat.getAD_ReportView().getAD_Table().getTableName();
			if (printFormat.getAD_ReportView().getWhereClause() != null)
				whereClause = " AND " + printFormat.getAD_ReportView().getWhereClause();
			if (printFormat.getAD_ReportView().getOrderByClause() != null)
				OrderByClause = printFormat.getAD_ReportView().getOrderByClause();

			reportViewCol = new Query(this.getCtx(), X_AD_ReportView_Col.Table_Name,
					"AD_ReportView_ID = ? AND AD_Client_ID = ? ", null)
							.setParameters(printFormat.getAD_ReportView().getAD_ReportView_ID(), getAD_Client_ID())
							.list();
		} else {

			tableName = new Query(Env.getCtx(), MTable.Table_Name, " AD_Table_ID = ? ", null)
					.setParameters(printFormat.getAD_Table_ID()).first().get_Value("TableName").toString();
		}

		List<MPrintFormatItem> formatItem = new Query(getCtx(), MPrintFormatItem.Table_Name,
				" AD_PrintFormat_ID = ? AND IsPrinted = ? ", null).setParameters(printFormat.get_ID(), "Y")
						.setOrderBy(" SeqNo ").list();

		sql.append("SELECT ");
		int items = 0;
		for (MPrintFormatItem formatI : formatItem) {
			items++;
			String formatType = formatI.getPrintFormatType();

			if (reportViewCol != null) {
				for (X_AD_ReportView_Col rwc : reportViewCol) {
					MColumn colum = MColumn.get(getCtx(), rwc.getAD_Column_ID());
					if (formatI.getColumnName().equalsIgnoreCase(colum != null ? colum.getColumnName() : "")) {
						replaceColumn = true;
						sql.append(rwc.getFunctionColumn());
					} else {
						//
					}
				}
			}

			if (!replaceColumn) {
				if (formatType.equals("F"))
					sql.append(formatI.getColumnName());
				else if (formatType.equals("T")) {
					sql.append("'");
					sql.append(formatI.getPrintName());
					sql.append("'");
				}
			} else {
				replaceColumn = false; // Inicializo.
			}

			if (formatItem.size() != items) {
				sql.append(", ");
				vSortNo.add(new KeyNamePair(formatI.getSortNo(), formatI.getColumnName()));

			}
			vMPrintFormatItem.add(formatI);
		}
		MPrintFormatItem[] itemsPF = new MPrintFormatItem[vMPrintFormatItem.size()];
		vMPrintFormatItem.toArray(itemsPF);

		sql.append(" FROM " + tableName);
		sql.append(" WHERE ");
		sql.append(tableName);
		sql.append("_ID = ");
		sql.append(p_Record_ID);
		sql.append(" " + whereClause);
		if (OrderByClause.length() > 0) {
			sql.append(" ORDER BY " + OrderByClause);
		}

		log.log(Level.INFO, "SQL: " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				line = new StringBuffer();
				for (int i = 1; i <= items; i++) {
					MPrintFormatItem item = itemsPF[i - 1];
					if (item.isNextLine())
						line.append("\r\n");
					line.append(rs.getString(i));
					if (i != items)
						line.append(p_FormatType);

				}

				line.append("\r\n");

				body.append(line.toString());
			}
			pstmt.close();

		} catch (Exception e) {
			log.warning(Msg.translate(Env.getCtx(), e.getMessage()));
			throw new AdempiereException(e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		try {
			java.io.FileWriter file = new java.io.FileWriter(p_file);
			java.io.BufferedWriter bw = new java.io.BufferedWriter(file);
			java.io.PrintWriter pw = new java.io.PrintWriter(bw);
			pw.write(body.toString());
			pw.close();
			bw.close();
		} catch (IOException ioe) {
			log.warning(Msg.translate(Env.getCtx(), ioe.getMessage()));
			throw new AdempiereException(ioe);
		}
		m_created++;
		return p_file;
	}

	private boolean toAttach(File p_file) throws IOException {
		boolean succefull = false;
		int AD_Table_ID = 0;
		if (p_file == null)
			return succefull;
		FileReader fr = new FileReader(p_file);
		BufferedReader buffer = new BufferedReader(fr);
		String linea = buffer.readLine();
		if (linea != null) {
			if (p_TableNameToAttach.length() > 0)
				AD_Table_ID = MTable.getTable_ID(p_TableNameToAttach);
			else
				AD_Table_ID = MTable.getTable_ID(tableName);
			log.log(Level.INFO, "AD_Table_ID: " + AD_Table_ID + " - Record_ID: " + p_Record_ID);
			MAttachment attach = MAttachment.get(getCtx(), AD_Table_ID, p_Record_ID);
			log.log(Level.INFO, "Contexto: " + getCtx().toString());

			log.log(Level.INFO, "AD_Table_ID: " + AD_Table_ID + " - Record_ID: " + p_Record_ID);
			attach = new MAttachment(getCtx(), AD_Table_ID, p_Record_ID, get_TrxName());
			attach.addEntry(p_file);
			attach.save();
			log.info("attach.save");
			succefull = true;
		} else {
			succefull = false;
		}
		buffer.close();
		return succefull;
	}
}
